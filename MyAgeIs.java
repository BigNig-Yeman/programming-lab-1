public class MyAgeIs {

    public static void main(String[] args) {
        // Prints ("Your age is" + int age) to the terminal window.
        int age;  
        int currentyear;
        int yearofbirth;

        currentyear = 2020;
        yearofbirth = 2001;
        age = currentyear - yearofbirth;    
        System.out.println("Your age is " + age);
        
        int base;
        int width;
        int perimeter;
        int area;
        base=2;
        width=4;
        perimeter= (2*base)+(2*width);
        area=base*width;
        System.out.println("The area of the rectangle is " + area);
        System.out.println("The perimeter of the rectangle is " + perimeter);

        double cad;
        double usd;
        double euro; 
        cad= 1;
        usd= cad*0.75;
        euro= cad*0.68;
        System.out.println(cad+" canadian dollar is equal to " +usd +" us dollars");
        System.out.println(cad+" canadian dollar is equal to " +euro +" euros"); 

        double feet;
        double meters;
        double value; 
        value= 1;
        meters= value*3.28; 
        feet= value*0.305;
        System.out.println(value+" meter(s) is equal to " +meters+ " feet");
        System.out.println(value+" foot/feet is equal to " +feet+ " meters");
    }

}
